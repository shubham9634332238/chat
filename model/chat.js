var mongoose = require('mongoose'),
schema = mongoose.Schema;
chatSchema = new schema({
	room:{type:String,unique:true},

	
messages:[	
{
	text:{type:String},
	postedby: {type:String, ref:'user'},
	date:{type:String},
	image:{type:String},
	toUser:{type:String ,ref:'user'},
}
]	
})
module.exports = mongoose.model('chat',chatSchema);

