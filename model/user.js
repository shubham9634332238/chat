var mongoose = require('mongoose'),
schema = mongoose.Schema;
userSchema = new schema({
	userName:{type:String,required:true,unique:true},
	password:{type:String},
	firstname:{type:String},
	lastname:{type:String},
	mobilenumber:{type:Number},
	pendingRequest:[
	{
		followerName:{type:String},
		firstname:{type:String}
	}
	],
	FriendList:[
	{
		friend:{type:String},
		firstname:{type:String},
		lastMessage:{type:String,default:" "},
		count:{type:Number,default:0},
		time:{type:String}
	}
	],
	islogin:{type:Boolean,default:false},
	room:{type:String}
	
	

})


module.exports = mongoose.model('user',userSchema);

