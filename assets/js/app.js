'use strict';

var app = angular.module('myApp', ['ngRoute', 'luegg.directives','myApp.service','myApp.chat']);
app.config(['$routeProvider', function($routeProvider){

	$routeProvider
	.when('/home', {
		templateUrl : 'templates/home.html',
		controller : 'homeCtrl'
	})
	.when('/login', {
		templateUrl : 'templates/login.html',
		controller : 'loginCtrl'
	})
	.when('/create-new-item', {
		templateUrl : 'templates/create-new-item.html',
		controller : 'createNewItemCtrl'
	})
	.when('/message-inbox/:roomId', {
		templateUrl : 'templates/message-inbox.html',
		controller : 'messageInboxCtrl'
	});

	$routeProvider.otherwise({redirectTo: '/home'});

}]);