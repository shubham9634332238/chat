'use strict';

  angular.module('myApp.service', ['ngRoute'])
  .service('myService',function($q,$http){
  self = this;
  this.abc=[];
  self.userInfo = {
                    "personal":{},
                    "relative":{},
                    "company":{},
                  }
  self.Signupdata={}
  self.userBasicInfo = {}
  self.email ={}
  self.viewUser = {}
  self.updateuser = {}
  self.username = {}
  self.googleAp = {}
  self.editProfile = {}
  self.yourInfo = {}

  //Post Method
  self.postMethod = function(data,AddedUrl){
  var def = $q.defer();
  $http({
    								method:"post",
    								url:AddedUrl,
    								"content-Type":"application/json",
    								data:JSON.stringify(data)
			     }).then(function(objS){
									  def.resolve(objS);
										}),function(objE){
										def.reject(objE);
							    	};
							      return def.promise;
            }
//Get Method
  self.getMethod = function(AddedUrl){
  var deff=$q.defer();
  var req = {
									  method: 'GET',
					        	url: AddedUrl,
								 	 "Content-Type": "application/json"
				  	};
  $http(req).success(function(objS){
							 		 deff.resolve(objS);
					      	 }).error(function(objE){
							  	 deff.reject({msg:objE});
									 });
									 return deff.promise;
	          }
            })


.filter('numSufx',function() {
  return function(number) {
      var lastDate = number%10;
      if(lastDate===1) {
          return number +'st';
      } else if(lastDate===2) {
          return number +'nd';
      } else if(lastDate===3) {
          return number +'rd';
      } else if(lastDate>3) {
          return number +'th';
      }
  }
})

.directive('myInfo',function() {
  return {
    template : '<div style="color:blue;" ng-repeat="data in myCustomer"> Name: {{data.name}} , Address : {{data.address}}<div>'
  }
})
