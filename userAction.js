// var jwt = require('jwt');
var express = require('express');
var user= require('./model/user')
var chat = require('./model/chat')
var app = express();
var socketio = require('socket.io');
var bcrypt = require('bcrypt')
var http = require('http')
var server = http.createServer(app)


var usernames = {};
var rooms = [];
var room;
var nicknames={}
clients=[];
exports.listen = function(server){
  io = socketio.listen(server);
   io.sockets.on('connection', function(socket){

   	console.log("connection established")


    socket.on('adduser', function (data) 
    {
      
        var username = data.username;
        
         room = data.room;

    
        if (rooms.indexOf(room) != -1) {
            socket.username = username;
            username=socket.username
               socket.room = room;
          
              usernames[username] = socket.id;
               socket.username=username;
          
               socket.join(room);
        console.log(io.sockets.adapter.rooms)
      
        } 
        else {
            socket.emit('updatechat', 'SERVER', 'Please enter valid code.');
        }
    });

     socket.on('addanotheruser', function (data) {
        var chatwith = data.chatwith;
        
         room = data.room;

            socket.chatwith = chatwith;
        if (rooms.indexOf(room) != -1) 
        {
              usernames[chatwith] = chatwith;
      } 
      else {
            socket.emit('updatechat', 'SERVER', 'Please enter valid code.');
        }
    });
    
    
    socket.on('createroom', function (data) {
      if(rooms.indexOf(data.room)==-1)
      { console.log("watching",data.room)
        rooms.push(data.room)
       bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(data.room, salt, function(err, hash) {
       //console.log(hash+"req--->>  "+data.room);
      })
  })
}       
        socket.emit('roomcreated', data);
       socket.emit('roomdata',data);
       });
      

       socket.on('sendchat', function (data)
        {
      
      var response=
        {
        "toUser":socket.chatwith,
        "username":socket.username
          }
        io.sockets.in(socket.room).emit('updatechat',response, data);
  
      }
    );

   
    socket.on('getmessages',function(data)
    {
         if(!data)
       {
        response=
         {
           responseCode:400,responseMessage:"please provide request body"}
          }
           else if(!data.room)
   
           {
             response=
             {
              responseCode:400,responseMessage:"please provide the roomspace detail"
            }
          }
       else
       {
         chat.findOne({room:data.room},function(err,result)
        {
          if(err)
          {
            response=
            {
            responseCode:400,responseMessage:"failed to show any message this time Please try again"}
            }
           else if(!result)
            {
              response=
             {
              responseCode:400,responseMessage:"There is no chat to show"
             }
        }
         else
          {
             response=
             {
            responseCode:200,responseMessage:"success",result:result}

            }
        socket.emit('showallmessages',response)
      })
    }
}
    )

    socket.on('counter',function(data)
    {
      socket.emit('unreadthemessagecounter',data)
    })


   socket.on('logout',function(data)
{
 
  if(!data)
  {
    res.json({responseCode:400,responseMessage:"something went wrong"})
  }
  else
  {
    user.findOneAndUpdate({userName:data.userName},{$set:{islogin:false}},function(err,result)
    {
      if(err)
      {
         data=
       {
           responseCode:400,
           responseMessage:"something went wrong" 
       }
      }
      else
      {
      // console.log(result)
       data=
       {
           responseCode:200,
           responseMessage:'successfully logout' 
       }
        delete usernames[socket.username]
       for(var i=0;i<rooms.length;i++)
       {

        // console.log(rooms[i])
        socket.leave(rooms[i])
       }
     
       socket.emit('logoutcompleted',data)
      }

    })
  }
}
)
    socket.on('disconnect', function (data) {

        delete usernames[socket.username];
  
        if (socket.username !== undefined) {
           for(var i=0;i<rooms.length;i++)
       {

        console.log(rooms[i])
        socket.leave(rooms[i])
       }
        }
    });
});

}


var async =require('async');

exports.getUserFromToken = function(headers, res){
	var token = headers.authorization;
	if(!headers || !headers.authorization){
		return res.json({code:400, message:"Unauthorized action."})
	}
	var decoded = jwt.decode(token, config.secret);
	return decoded._doc;
}






exports.signup= function(req,res)
{
	if(!req.body)
	{
		res.json({
			responseCode:400,
			responseMessage:"Please provide Details"
		})
	}
		else if(!req.body.userName)
		{
			res.json({
			responseCode:400,
			responseMessage:"please provide userName"
		})
		}
		else if(!req.body.password)
		{
			res.json({
              responseCode:400,
			responseMessage:"please provide Password"
		})
		}
		 else
		   {
			var User = new user(req.body)
			User.save(function(err,result)
			{
				if(err)
				{
					res.json({
					responseCode:400,
			       responseMessage:"Something went wrong please try again"
			   })
				}
				
				{
					res.json({
                  responseCode:200,
			      responseMessage:"You have successfully logged in", 
			      result:result
			  })


				}

			})
		}
	}

exports.login = function(req,res)
{
	console.log(req.body)
	if(!req.body)
	{
		return res.json({responseCode:400,responseMessage:"please enter username and password"})
	}
	else
	{
	 user.findOneAndUpdate({userName:req.body.userName,password:req.body.password},{$set:{islogin:true}},function(err,data)
		{
			if(err)
			{
				return res.json({responseCode:400,responseMessage:"something went wrong"})
			}
			else if(!data)
			{
				return res.json({responseCode:400,responseMessage:"Invalid username and password"})
			}
			else
			{
				return res.json({responseCode:200,responseMessage:"successfully logged in",result:data})
			}
		})
		
}
}

 exports.sendRequest = function(req,res)
 {
 	if(!req.body)
 		{
 			res.json({responseCode:400,responseMessage:"Please provide details"})
 		}
 		else
 		{
 			console.log(req.params.userName)
 			console.log(req.body.userName)

 		 user.findOneAndUpdate({userName:req.params.userName},{$push:{pendingRequest:{followerName:req.body.userName,firstname:req.body.firstname}}},{new:true},function(err,data){
 		 	if(err)
 		 	{
 		 		res.json({responseCode:400,responseMessage:"something went wrong"

 		 	})
 		 	}
 		 	else
 		 	{
                 res.json({responseCode:200,responseMessage:"success", result:data})
 		 	}
 		 })
 		}
 }


 exports.acceptrequest =function(req,res)
{
 	if(!req.body)
 		{
 			res.json({responseCode:400,responseMessage:"Please provide details"})
 		}
 		else
 		{ 
 			 async.waterfall([
 			 	function(callback)
 			 	{
 		      user.findOneAndUpdate({userName:req.body.userName},{$pull:{pendingRequest:{friend:req.params.userName,firstname:req.params.firstname}}},function(err,data){
 		 	  if(err)
 		 	  {
 		 		callback(err)

 		 	  
 		 	  }
 		 	  else
 		 	  {
                 callback(null,data)
 		 	   }
 		   })
 		},
 		function(data,callback)
 		{
 			user.findOneAndUpdate({userName:req.body.userName},{$push:{FriendList:{friend:req.params.userName,firstname:req.params.firstname}}},function(err,result){
 				if(err)
 				{
 					callback(err)
 				}
 				else
 					callback(null,data,result)


 			})
 		},
 		function(data,result,callback)
 		{
 			user.findOneAndUpdate({userName:req.params.userName},{$push:{FriendList:{friend:req.body.userName,firstname:req.body.firstname}}},function(err,results){
 				if(err)
 				{
 					callback(err)
 				}
 				else
 					callback(null,data,result,results)


 			})
 		}
 		],
 		function(err,data,result,results)
 		{
 			if(err)
 			{
 				res.json({responseCode:400,responseMessage:"something went wrong"})
 			}
 			else
 			{
 				res.json({responseCode:200,responseMessage:"friend request accepted",result:result})
 			}
 		})

 		}
 	}
 

 exports.getInfo = function(req,res)

 {
 	console.log('getInfo')
 	if(!req.body)
 	{
 		res.json({responseCode:400,responseMessage:"Please provide some request"})
 	}
 	else
 	{
 		user.findOne({userName:req.body.userName},{password:0},function(err,result){
 			if(err)
 			{
 				res.json({responseCode:400,responseMessage:"something went wrong"})
 			}
 			else
 			{
 				res.json({responseCode:200,responseMessage:"UserINFO HAS BEEN PROVIDED",result:result})
 			}
 		})
 	}
 }


 exports.savemsgdetails = function(req,res)
 {
 	var i=0;

 	if(!req.body)
 	{
 		res.json({responseCode:400,responseMessage:"please enter message"})
 	}  
   else
   {
       var obj={}
       obj.text=req.body.text;
       obj.postedby=req.body.postedby;
       obj.date=req.body.date;
       obj.image= req.body.image;
       obj.toUser=req.body.toUser
 		
   	var message =[];
   	message.push(obj)
   	console.log(message)
   
   	chat.findOne({room:req.body.room},function(err,result){
   		if(!result)
   		{
   			console.log('hello')
   			var Chat = new chat({
   		room:req.body.room,
   		messages:message})
   		 Chat.save(function(err,result0)
   		 {
   		 	if(err)
   		 	{
   		 		res.json({responseCode:400,responseMessage:"Something went wrong"})
   		 	}
   		 	else
   		 	{
   		 		res.json({responseCode:200,responseMessage:"chat has been succesfully saved",result:result0})
   		 	}
   		 })
   		}
   		 	else
   		 	{
   		 		chat.findOneAndUpdate({room:req.body.room},{$push:{messages:obj}},function(err,result1)
   		 		{
   		 			if(err)
   		 			{
   		 				res.json({responseCode:400,responseMessage:"something went wrong"})
   		 			}
   		 			else
   		 			{
   		 				
   		 				res.json({responseCode:200,responseMessage:"message has been successfully saved",result:result1})
   		 			}
   		 		})
   		 	}
           
   		 
   		
   	})
  }
 }

exports.getmessages = function(req,res)
{
	if(!req.body)
	{
		res.json({responseCode:400,responseMessage:"please provide request body"})
	}
	else if(!req.body.room)
		{res.json({responseCode:400,responseMessage:"please provide the roomspace detail"})
		}
		else
		{
			chat.findOne({room:req.body.room},function(err,result)
			{
				if(err)
				{
					res.json({responseCode:400,responseMessage:"failed to show any message this time Please try again"})
				}
				else if(!result)
				{
					res.json({responseCode:400,responseMessage:"There is no chat to show"})
				}
				else
				{
					res.json({responseCode:200,responseMessage:"success",result:result})
				}
			})
		}
}

exports.savelastmessagedetails= function(req,res)
{
  if(!req)
  {
    res.json({responseCode:400,responseMessage:"please provide request body"})
  }
  else
  {
    user.findOne({userName:req.body.loginuser},function(err,result)
    {
      if(err)
      {
       return res.json({responseCode:200,responseMessage:"something went wrong"})
      }
      else
      {
        //console.log("534",req.body)
        console.log(result.FriendList.length)
        for(var i = 0;i<result.FriendList.length;i++)
        {
       if(result.FriendList[i].friend==req.body.friend)
       {
        if(!req.body.message)
        {
          req.body.message=result.FriendList[i].lastMessage;
        }
     var FriendList=[]
     result.FriendList.splice(i,1,{"firstname":req.body.firstname,"friend":req.body.friend,"lastMessage":req.body.message,"count":req.body.count,"time":req.body.time})
      FriendList=result.FriendList
          
        user.findOneAndUpdate({userName:req.body.loginuser},{$set:{FriendList}},{new:true},function(err,resultant)
        {
          if(err)
            {
             return res.json({responseCode:400,responseMessage:"something went wrong"})
            }
            else
            {
             return res.json({responseCode:200,responseMessage:"last message updated",result:resultant})  
            }

        })
       }
      }
      }

    })
  }
}
